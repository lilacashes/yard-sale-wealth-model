from statistics import mean

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import json
from datetime import datetime
from operator import itemgetter
from random import randint, random

"""
    Model an economy on the yard sale model
    http://www.scientificamerican.com/article/is-inequality-inevitable/    
"""

MAX_STEPS = 1000_000
STEPS_BETWEEN_PRINTS = MAX_STEPS / 10
NUM_ACTORS = 1_000
WIN_PERCENTAGE = 1.2
LOSS_PERCENTAGE = 0.87
DELTA_W = 0.2
# CHI = 0.0004  # about reproduces the US wealth distribution from 2007
CHI = 0.036
ZETA = 0.050


class Model:

    def __init__(
            self, num_actors: int,
            win_percentage: float = WIN_PERCENTAGE, loss_percentage: float = LOSS_PERCENTAGE,
            delta_w: float = DELTA_W, chi: float = CHI, zeta: float = ZETA
    ):
        self.num_actors = num_actors
        self.win_percentage = win_percentage
        self.loss_percentage = loss_percentage
        self.delta_w = delta_w
        self.chi = chi
        self.zeta = zeta
        self.actors = [1.] * num_actors
        self.average_wealth = float(num_actors)
        self.instant_average_recalculation = True

    def random_transaction(self):
        self.transaction(randint(0, self.num_actors - 1), randint(0, self.num_actors - 1))

    def transaction(self, actor1: int, actor2: int):
        if actor1 == actor2:
            return
        poorer = min(self.actors[actor1], self.actors[actor2])
        transaction_value = self.delta_w * poorer
        if self.first_wins(actor1, actor2):  # actor1 wins
            transaction_amount = self.win_percentage * transaction_value
        else:  # actor1 loses
            transaction_amount = -self.loss_percentage * transaction_value
        self.perform_transaction(actor1, actor2, transaction_amount)

        self.chi_correction(actor1)
        self.chi_correction(actor2)
        if self.instant_average_recalculation:
            self.update_average_wealth()

    def first_wins(self, actor1: int, actor2: int) -> bool:
        score1 = random()
        score2 = random() + (actor2 - actor1) / self.average_wealth * self.zeta
        return score1 > score2

    def perform_transaction(self, actor1, actor2, transaction_amount):
        self.actors[actor1] += transaction_amount
        self.actors[actor2] -= transaction_amount

    def update_average_wealth(self):
        if self.chi:
            self.average_wealth = mean(self.actors)

    def chi_correction(self, actor):
        """
        Have each agent take a step toward the mean wealth in the society after each transaction
        The size of the step is some fraction χ (or “chi”) of their distance from the mean
        """
        self.actors[actor] += (self.average_wealth - self.actors[actor]) * self.chi

    def kappa_correction(self):
        """
        So we introduced a third parameter, κ (or “kappa”), which shifts the wealth distribution
        downward, thereby accounting for negative wealth. We supposed that the least wealth the
        poorest agent could have at any time was –S, where S equals κ times the mean wealth. Prior
        to each transaction, we loaned wealth S to both agents so that each had positive wealth.
        They then transacted according to the extended yard sale model, described earlier, after
        which they both repaid their debt of S.
        """
    def print_state(self, step: int):
        if not self.instant_average_recalculation:
            self.update_average_wealth()
        by_wealth = sorted(
            [(i, self.actors[i]) for i in range(self.num_actors)], key=itemgetter(1)
        )
        bottom_half = sum(
            actor[1] for actor in by_wealth[0:self.num_actors // 2]
        ) / self.average_wealth
        bottom_90_percent = sum(
            actor[1] for actor in by_wealth[0:9 * self.num_actors // 10]
        ) / self.average_wealth
        bottom_99_percent = sum(
            actor[1] for actor in by_wealth[0:99 * self.num_actors // 100]
        ) / self.average_wealth
        print(f"Step: {step} (avg. {step // self.num_actors} transactions per actor)")
        to_print = [
            f"{a[0]}: {100 * a[1] / self.num_actors / self.average_wealth:.3f}%"
            for a in by_wealth[-1:-6:-1]
        ]
        print(f"  Richest:   {'   '.join(to_print)}")
        print(f"  Bottom 50%: {100 * bottom_half / self.num_actors:.3f}%"
              f"  Bottom 90%: {100 * bottom_90_percent / self.num_actors:.3f}%"
              f"  Bottom 99%: {100 * bottom_99_percent / self.num_actors:.3f}%")
        print(f"  Average: {self.average_wealth}")


def main():
    model = Model(NUM_ACTORS)
    distribution = []

    for step in range(1, MAX_STEPS + 1):
        model.random_transaction()

        if step % STEPS_BETWEEN_PRINTS == 0:
            model.print_state(step)
            distribution.append(model.actors[:])

    with open(f'wealth_distribution.{datetime.now().strftime("%m-%d-%H:%M")}.json', 'w') as outfile:
        json.dump(distribution, outfile)


if __name__ == "__main__":
    main()
